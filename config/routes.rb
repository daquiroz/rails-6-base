Rails.application.routes.draw do


  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  devise_scope :user do
      get 'users/sign_out' => "devise/sessions#destroy"
  end

  scope "(:locale)", locale: /en|es|pt/  do
    get 'home/index'
    root 'pages#home'
    resources :articles
    resources :resources
    get 'pages/contact'
    get 'pages/home'
  end

end
