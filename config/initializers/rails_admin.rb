RailsAdmin.config do |config|

  ### Popular gems integration
  config.parent_controller = 'ApplicationController'

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == CancanCan ==
  config.authorize_with :cancancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.included_models = [
  'Article',
  'User',
  'Article::Translation',
  'Resource',
  'Resource::Translation',
  'Member',
  'Member::Translation',
  ]

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  ### Locale Visibility and UX ###

  config.model 'Article' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :title
    end
  end

  config.model 'Article::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :summary, :text
  end

  config.model 'Resource' do
    configure :translations, :globalize_tabs
    list do
      field :id
      field :title
    end
  end

  config.model 'Resource::Translation' do
    visible false
    configure :locale, :hidden do
      help ''
    end
    include_fields :locale, :title, :description
  end

end
