class Ability
  include CanCan::Ability

  def initialize(user)
    if user.has_role? :admin
      can :manage, :all
      can :dashboard, :all
      can :access, :rails_admin
    elsif user.has_role? :editor
      can :manage, :all
      can :dashboard, :all
      can :access, :rails_admin
    end

  end
end
