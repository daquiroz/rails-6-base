class Article < ApplicationRecord
  translates :title, :text, :summary
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_one_attached :image

end
