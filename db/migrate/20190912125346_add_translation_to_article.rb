class AddTranslationToArticle < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Article.create_translation_table! :title => :string, :summary => :text, :text => :text
      end

      dir.down do
        Article.drop_translation_table!
      end
    end
  end
end
