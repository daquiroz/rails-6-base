class AddTranslationToResource < ActiveRecord::Migration[6.0]
  def change
    reversible do |dir|
      dir.up do
        Resource.create_translation_table! :title => :string, :description => :text
      end

      dir.down do
        Resource.drop_translation_table!
      end
    end
  end
end
