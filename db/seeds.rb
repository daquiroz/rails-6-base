# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


require 'faker'

p 'Create users 👩‍💻'

u1 = User.new
u1.email = "admin@a.org"
u1.password = "admina"
u1.save
u1.add_role "admin"

u2 = User.new
u2.email = "editor@a.org"
u2.password = "editora"
u2.save
u2.add_role "editor"

u3 = User.new
u3.email = "author@a.org"
u3.password = "authora"
u3.save
u3.add_role "author"

p 'Users created ✔'

p 'Create articles 🗞'

10.times do |article|
  I18n.locale = :en
  a = Article.find_or_create_by(
    title: Faker::Quote.famous_last_words,
    text: Faker::Lorem.paragraphs(number: 4)[0],
    summary: Faker::Lorem.paragraphs(number: 1)[0],
  )
  path_image = '/public/seeds/art' + rand(1..6).to_s + '.png'
  a.image.attach(io: File.open(File.join(Rails.root, path_image)), filename: a.title)

  I18n.locale = :es
  a.update(
    title: Faker::Quote.famous_last_words,
    text: Faker::Lorem.paragraphs(number: 4)[0],
    summary: Faker::Lorem.paragraphs(number: 1)[0],
  )

  I18n.locale = :pt
  a.update(
    title: Faker::Quote.famous_last_words,
    text: Faker::Lorem.paragraphs(number: 4)[0],
    summary: Faker::Lorem.paragraphs(number: 1)[0],
  )
end

p 'Articles created ✔'




p 'Create resources 📷'

10.times do |resource|
  I18n.locale = :en
  a = Resource.find_or_create_by(
    title: Faker::Book.title,
    description: Faker::Lorem.paragraphs(number: 4)[0],
  )

  path_image = '/public/seeds/res' + rand(1..4).to_s + '.png'
  a.attach.attach(io: File.open(File.join(Rails.root, path_image)), filename: a.title)
  a.image.attach(io: File.open(File.join(Rails.root, path_image)), filename: a.title)

  I18n.locale = :es
  a.update(
    title: Faker::Book.title,
    description: Faker::Lorem.paragraphs(number: 4)[0],
  )

  I18n.locale = :pt
  a.update(
    title: Faker::Book.title,
    description: Faker::Lorem.paragraphs(number: 4)[0],
  )
end

p 'Resources created ✔'

p 'Create members'

10.times do |member|
  m = Member.find_or_create_by(
    name: Faker::Movies::Hobbit.thorins_company,
    link: 'ciudadaniai.org',
  )
  path_image = '/public/seeds/logo-fci.png'
  m.logo.attach(io: File.open(File.join(Rails.root, path_image)), filename: m.name)
end

p 'Members created ✔'
